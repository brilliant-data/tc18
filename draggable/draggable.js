

// misc stuff

$(function(){

function px(v) { return parseInt(v) + 'px'; }


let DROPDOWN_STYLES = [
  {
    height: '347px',
    opacity: 1.0,
    "box-shadow": "5px 5px 15px rgba(0,0,0,0.2)",
    'background-color': 'white',

  },
  {
    height: '32px',
    opacity: 0.7,
    "box-shadow": "2px 2px 5px rgba(0,0,0,0.1)",
    'background-color': '#777',
  },
];

function makeDropdown(target, styles = DROPDOWN_STYLES ) {
  let index = 0;


  function toggle(target){
    index = (index + 1) % styles.length;
    target.css(styles[index])
  }


  target
    .off('dblclick')
    .on('dblclick', function(e){
      toggle(target)
      console.log(target);
    })

}



function makeDragable(target) {
  let dragStartPos = null;
  let offsets = { x: 0, y: 0 };

  // comp wacky Tableau coordinates
  let TABLEAU_OFFSET_COMPENSATION_Y = 24;


  target
  .attr({draggable: "true"})
  .off('dragstart')
  .on('dragstart', function(e){
    // store offset 
    offsets = { x: e.offsetX, y: e.offsetY };
  })
  .off('dragend')
  .on('dragend', function(e){
    target.css({
      left: e.pageX - offsets.x,
      top: e.pageY - offsets.y - TABLEAU_OFFSET_COMPENSATION_Y,
    })
  })

}

window.parent.$('div')
  .filter(function(i) {
    return $(this).attr( "id" ) && $(this).attr( "id" ).startsWith('tabZoneId');
  }).each(function(i) {

    makeDragable( $(this));
    makeDropdown( $(this), DROPDOWN_STYLES);
 });

});

